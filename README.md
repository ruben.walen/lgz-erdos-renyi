# LGZ-Erdos-Renyi

LGZ algorithm for Erdos-Renyi graphs. Virgil Woerdings & Ruben Walen, Quantum Algorithms 2020-2021.
- cliques.py: contains clique-generation functions and simplicial complex handling, including boundary map generation
- clique_distros.py: plot clique distributions
- boundaries.py: basic boundary map checks
- ER_stats.py: some plotting for Erdos Renyi graphs and exploring its clique complexes
- sample_dataset.py: code to generate a circular-shape dataset with a hole, and shows its Vietoris Rips
- bettinumbers.py: code to plot some Betti numbers for Erdos Renyi graphs with numpy.linalg.svd
- Hamiltonian simulation + qpe.ipynb: HS-QPE Lloyd-Garnerone-Zanardi algorithm implementation using cirq
- matrix to nullity.ipynb: nullity estimation using LGZ
import networkx as nx
import matplotlib.pyplot as plt
import math
import random
from cliques import *

#G = nx.erdos_renyi_graph(50, 0.05)
#print([len(x) for x in nx.connected_components(G)])
#nx.draw(G)
#plt.show()

# Percolation graph
disable = False
if not disable:
    SIZE = 20
    NUM = 100
    DUPLO = 20
    P_MAX = 0.4

    x_val = []
    y_val = []
    y_stdevs = [] # error bars
    for i in range(NUM):
        p = P_MAX * (i / NUM)
        curr = []
        for j in range(DUPLO):
            G = nx.erdos_renyi_graph(SIZE, p)
            s_largest = sorted([len(x) for x in nx.connected_components(G)], reverse=True)[0]
            curr.append(s_largest)
        x_val.append(p)
        avg = sum(curr) / DUPLO
        y_val.append(avg)
        y_stdevs.append(math.sqrt(sum([(v - avg)**2 for v in curr]) / DUPLO))

    fig, ax = plt.subplots()
    ax.errorbar(x_val, y_val, yerr=y_stdevs)
    ax.set_xlabel("Edge retention probability")
    ax.set_ylabel("Largest connected component size")
    ax.set_title("LCC size for Erdos-Renyi graph of size " + str(SIZE))
    plt.show()

# Basic clique computations
disable = False
if not disable:
    G = nx.erdos_renyi_graph(10, 0.3)
    clique = maximalCliqueExtension(0, G)[0]
    print(len(clique), clique)
    pos = nx.spring_layout(G)
    cmap = ['b' for _ in range(len(G.nodes))]
    for c in clique:
        cmap[c] = '#FF0000'
    nx.draw_networkx(G, pos=pos, node_color=cmap)
    plt.show()

    def randomHexColor(seed):
        st = random.getstate()
        random.seed(seed)
        col = '#%02x%02x%02x' % (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        random.setstate(st)
        return col

    clq_dict = {}
    e_dict = {}
    for n in G.nodes:
        clql = maximalCliqueExtension(n, G, return_all_same_size=False, return_for_all_edges=True)
        clq_dict[n] = clql
        for clq in clql:
            for k in clq:
                e_dict[(n, k)] = n
                e_dict[(k, n)] = n
        
    e_cmap = [] # edge colormap
    for e in G.edges:
        if (e[0], e[1]) not in e_dict.keys():
            col = 'b'
        else:
            #col = randomHexColor(e_dict[(e[0], e[1])])
            col = 'r'
        e_cmap.append(col)

    print([(n, [len(c) for c in clq_dict[n]]) for n in clq_dict.keys()])
    nx.draw_networkx(G, pos=pos, edge_color=e_cmap)
    plt.show()

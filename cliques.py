"""Clique and simplicial computation functions & misc tools"""

import networkx as nx
import math
import random
import time
import numpy as np

def timeFunction(self, n_execs, function, *args, **kwargs):
    """Time a function (execution time)."""
    results = []
    for i in range(n_execs):
        start = time.time()
        returns = function(*args, **kwargs)
        end = time.time()
        results.append(end - start, returns)
    return results

def binomial(n, k):
    if n > 0 and k > 0 and k <= n:
        return math.factorial(n) / math.factorial(k) / math.factorial(n - k)
    else:
        raise ValueError("@cliques.binomial: wrong argument values.")

def matrixNullity(matrix):
    """Compute matrix nullity with SVD."""
    if matrix.shape[0] == 0 or matrix.shape[1] == 0:
        return 0
    
    try:
        eigenvalues = np.linalg.svd(matrix)[1]
        nullity = sum(np.isclose(eigenvalues, 0)) / len(eigenvalues)
    except np.linalg.LinAlgError:
        print("@cliques.matrixNullity: SVD convergence failure")
        nullity = float('nan')
    return nullity

def matrixZeroPadding(matrix, h_padding, v_padding):
    """Pad a matrix with zeros horizontally and/or vertically.
        Horizontal padding: add columns
        Vertical padding: add rows"""
    new_matrix = matrix.copy()
    if h_padding > 0:
        hzeros = np.zeros((new_matrix.shape[0], h_padding))
        new_matrix = np.hstack(new_matrix, hzeros)
    if v_padding > 0:
        vzeros = np.zeros((v_padding, new_matrix.shape[1]))
        new_matrix = np.vstack(new_matrix, vzeros)
    return new_matrix

def maximalCliqueExtension(node, G, return_all_same_size=False, return_for_all_edges=False):
    """Extend a maximal clique from node in G.
        Note: a node can be involved in more than one local maximal clique.
        return_all_same_size (bool): whether to return all maximal cliques of the same size
        return_for_all_edges (bool): whether to return maximal cliques such that at least every neighbour is represented
            (no effect with return_all_same_size)"""
    neighbours_to_check = set(G[node])
    n_clique = []
    while not len(neighbours_to_check) == 0:
        n_clique_add = set([])
        b = random.sample(neighbours_to_check, 1)[0] # sample random neighbour
        n_clique_add.add(b) # add to current clique
        for c in set(G[node]): # loop over addable neighbours
            if c != b and c != node: # counter self-loops?
                add = True
                for h in n_clique_add: # check all in current clique
                    if c not in G[h]:
                        add = False
                if add == True:
                    n_clique_add.add(c) # add c to clique
        for k in n_clique_add: # discard from checkable neighbours
            neighbours_to_check.discard(k)
        if len(n_clique) == 0: # initial clique
            n_clique = [n_clique_add]
        else:
            if return_for_all_edges: # always add edge maximal clique
                n_clique.append(n_clique_add)
            else:
                if len(n_clique_add) > len(n_clique[0]): # if larger clique: update
                    n_clique = [n_clique_add]
                if return_all_same_size: # return all of same size
                    if len(n_clique_add) == len(n_clique[0]):
                        n_clique.append(n_clique_add)

    for clq in n_clique:            
        clq.add(node) # add node itself
    if len(n_clique) == 0: # single-vertex cliques
        n_clique = [set([node])]
        
    return n_clique

def combinations(iterable, r):
    """r-Combinations of iterable.
        Adapted from: https://docs.python.org/3/library/itertools.html#itertools.combinations"""
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = list(range(r))
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)

class SimplicialCollection:
    """This class stores a collection of simplices (not necessarily the whole complex).
        Intent is to store maximal size (tree-top) simplices here.
        NOTE: a k-simplex has k+1 nodes."""
    def __init__(self):
        self.clear()
        return

    def clear(self):
        self.simplices = []
        self.simplex_sizes = []
        self.k_simplex_dict = {}
        self.graph = None
        return

    def constructFromGraph(self, graph, return_all_same_size=False, return_for_all_edges=False):
        """Construct a top-level maximal simplicial collection (without duplicates) from a graph."""
        simplices_list = []
        repr_dict = {}
        for n in graph.nodes:
            clq = maximalCliqueExtension(n, graph, return_all_same_size=return_all_same_size, return_for_all_edges=return_for_all_edges)
            for c in clq:
                try:
                    a = repr_dict[frozenset(c)]
                except KeyError:
                    simplices_list.append(c)
                    repr_dict[frozenset(c)] = True
                    
        self.simplices = simplices_list # listed simplices
        self.simplex_sizes = [len(s) for s in self.simplices] # simplex sizes by index (not k !)
        self.k_simplex_dict = {k : [] for k in range(graph.number_of_nodes())} # dictionary of k-simplices stored by self.simplices index
        for i, ks in enumerate(self.simplex_sizes):
            self.k_simplex_dict[ks-1].append(i)
        self.graph = graph
        return simplices_list

    def getBinaryCode(self, simplex):
        """Get integer code for a given simplex by summing with binary exponents for each vertex position.
            (only works with integer node names)"""
        value = 0
        for n in simplex:
            value += 2**n # RAISE: ValueError if not int
        return value

    def simplexFromBinaryCode(self, code):
        """Get the simplex pertaining to binary code (integer)."""
        N = self.graph.number_of_nodes()
        if type(code) != int:
            raise ValueError("@SimplicialCollection.simplexFromBinaryCode: code value must be integer.""")
        if code >= 2**(N+1):
            raise ValueError("@SimplicialCollection.simplexFromBinaryCode: code value exceeds maximum for graph.""")
        if code < 0:
            raise ValueError("@SimplicialCollection.simplexFromBinaryCode: code value smaller than zero.""")
        
        if code == 0:
            return set([])

        remaining_code = code
        exp_list = [(i, 2**i) for i in range(N)] # 2^n values
        vertices = set([]) # set of vertices in simplex
        while True:
            ind = -1 # RAISE: list index out of range if something goes wrong below
            for ind, (i, exp) in enumerate(exp_list):
                if exp > remaining_code:
                    sel = i - 1 # select previous exponent for inclusion
                    break
            vertices.add(sel) # append this vertex
            exp_list.pop(ind) # binary dense code: only includes each index at most once, don't check again
            remaining_code -= exp_list[ind][1]
            if remaining_code == 0:
                break
        return vertices

    def subSimplices(self, simplex, k):
        """Sub-simplices (faces) of size k of simplex."""
        return [c for c in combinations(simplex, k+1)]

    def countSubSimplices(self, simplex, k):
        """Same as subSimplices, but only counts number of simplices."""
        # TODO: this is just a binomial
        count = 0
        for c in combinations(simplex, k+1):
            count += 1
        return count

    def overlap(self, simplex1, simplex2, *args):
        """Overlapping simplex (face) between simplex1 and simplex2, optionally more simplices."""
        return simplex1.intersection(simplex2, *args)

    def obverseNaiveSimplexCount(self, k, return_simplices=False):
        """Obverse naive count of k-simplices (k+1 nodes) in collection.
            Counts based on simplices in collection.
            return_simplices (bool): whether to return the found simplices"""
        code_dict = {} # dictionary keyed by bincodes: which simplices have already been found (naive)
        count = 0
        if return_simplices:
            k_simplices = []
        for i in self.k_simplex_dict[k]: # direct k-simplices, don't have to check for occurence
            s = self.simplices[i]
            code_dict[self.getBinaryCode(s)] = True
            count += 1
            if return_simplices:
                k_simplices.append(s)
        for k_l in range(k+1, self.graph.number_of_nodes()): # faces of larger simplices
            for i in self.k_simplex_dict[k_l]:
                s = self.simplices[i]
                for sub_s in self.subSimplices(s, k):
                    try: # already found
                        a = code_dict[self.getBinaryCode(sub_s)]
                    except KeyError: # not already found
                        count += 1
                        code_dict[self.getBinaryCode(sub_s)] = True
                        if return_simplices:
                            k_simplices.append(sub_s)
                            
        if not return_simplices:
            return count
        else:
            return count, k_simplices

    def kBoundaryMap(self, k):
        """Return the boundary map from k to k-1 using obverse simplex enumeration.
            The boundary map has shape (n (k-1)-simplices, n k-simplices
            Map every [10000...] basis vector for k-smp to k-1-smp boundaries"""
        if k < 0:
            raise ValueError("@SimplexCollection.kBoundaryMap: value of k must be at or above 0")
        if k >= self.graph.number_of_nodes():
            raise ValueError("@SimplexCollection.kBoundaryMap: value of k must be smaller than size of graph in nodes - 1")
        if k == 0: # the zero vector for k = 0
            print(">SimplicialCollection.kBoundaryMap: k=0 boundary map is a zero-vector!")
            return np.zeros((1, self.graph.number_of_nodes()))
                            
        ck, k_list = self.obverseNaiveSimplexCount(k, return_simplices=True)
        cksub, ksub_list = self.obverseNaiveSimplexCount(k-1, return_simplices=True)

        k_codes = sorted([(i, self.getBinaryCode(s)) for i, s in enumerate(k_list)], key=lambda t: t[1]) # binary codes for sorting
        ksub_codes = sorted([(i, self.getBinaryCode(s)) for i, s in enumerate(ksub_list)], key=lambda t: t[1]) # sort by code
        ksub_codes_dict = {} # having this as a dict is easier
        for i, tup in enumerate(ksub_codes):
            ksub_codes_dict[tup[1]] = i # binary code for k-1 --> order index

        bmatrix = np.zeros((cksub, ck)) # (k-1, k)
        for i, tup in enumerate(k_codes): # for k-simplices, in order
            s = k_list[tup[0]] # the simplex itself
            sub_list = self.subSimplices(s, k-1) # its (k-1)-simplices
            for j, sub in enumerate(sub_list): # sub-simplex (face)
                order_ksub = ksub_codes_dict[self.getBinaryCode(sub)] # index in ksub_codes: order in list = index in matrix
                bmatrix[order_ksub, i] = [-1, 1][j % 2 == 0] # if j = 0, 2, etc. --> +1 else -1
        return bmatrix

    def kCombinatorialLaplacian(self, k):
        """Return the combinatorial for k using obverse simplex enumeration (see kBoundaryMap).
            Its shape is (n k-simplices, n k-simplices)"""
        if k < 0:
            raise ValueError("@SimplexCollection.kCombinatorialLaplacian: value of k must be at or above 0")

        if k != 0:
            ku_matrix = self.kBoundaryMap(k+1)
            matrix_upper = np.matmul(ku_matrix, ku_matrix.T)
            del ku_matrix
            kd_matrix = self.kBoundaryMap(k)
            matrix_lower = np.matmul(kd_matrix.T, kd_matrix)
            del kd_matrix
            return matrix_lower + matrix_upper
        else:
            ku_matrix = self.kBoundaryMap(k+1)
            matrix_upper = np.matmul(ku_matrix, ku_matrix.T)
            return matrix_upper
            
                
        

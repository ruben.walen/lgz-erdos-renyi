"""Compute clique distributions for Erdos-Renyi graphs"""

import networkx as nx
import matplotlib.pyplot as plt
import math
import random
from cliques import *

# Compute clique distribution
SIZE = 20 # graph size
NUM = 10 # number of slices for p
DUPLO = 10 # number of experiments
P_MAX = 1 # max p value
K_MIN, K_MAX = 1, 6 # max simplex sizes (k-simplex = k+1 nodes)
BINOMIAL_NORMALISER = True # normalize by Binom(n, k+1)
# this is the maximum number of k-simplices
INVERT_COUNT = False # invert count (clique density form)
ERROR_BARS = True
DEPEND_ON_N = True # switch from p to N as param instead
N_MIN, N_MAX = 7, 30
P_VAL = 0.3 # p-value for this plot

if N_MIN <= K_MAX and DEPEND_ON_N:
    raise ValueError("@clique_distros: N_MIN must be larger than K_MAX if DEPEND_ON_N")

x_val = []
y_vals = [[] for k in range(K_MIN, K_MAX+1)]
y_stdevs = [[] for k in range(K_MIN, K_MAX+1)]
max_clique = SIZE
if not DEPEND_ON_N:
    for i in range(NUM+1):
        p = P_MAX * (i / NUM)
        curr = [[] for k in range(K_MIN, K_MAX+1)]
        for j in range(DUPLO):
            G = nx.erdos_renyi_graph(SIZE, p)
            sc = SimplicialCollection()
            sc.constructFromGraph(G, return_for_all_edges=True)
            for k in range(K_MIN, K_MAX+1):
                print(p, k)
                count = sc.obverseNaiveSimplexCount(k, return_simplices=False)
                if BINOMIAL_NORMALISER:
                    count = count / binomial(SIZE, k+1)
                if INVERT_COUNT:
                    if count != 0:
                        count = 1 / count
                    else:
                        count = float('nan')
                curr[k-K_MIN].append(count)
        x_val.append(p)
        for k in range(K_MIN, K_MAX+1):
            avg = sum(curr[k-K_MIN]) / DUPLO
            y_vals[k-K_MIN].append(avg)
            if ERROR_BARS:
                y_stdevs[k-K_MIN].append(math.sqrt(sum([(v - avg)**2 for v in curr[k-K_MIN]]) / DUPLO))
else:
    for s in range(N_MIN, N_MAX+1): # size of graph
        curr = [[] for k in range(K_MIN, K_MAX+1)]
        for j in range(DUPLO):
            G = nx.erdos_renyi_graph(s, P_VAL)
            sc = SimplicialCollection()
            sc.constructFromGraph(G, return_for_all_edges=True)
            for k in range(K_MIN, K_MAX+1):
                print(s, k)
                count = sc.obverseNaiveSimplexCount(k, return_simplices=False)
                if BINOMIAL_NORMALISER:
                    count = count / binomial(s, k+1)
                if INVERT_COUNT:
                    if count != 0:
                        count = 1 / count
                    else:
                        count = float('nan')
                curr[k-K_MIN].append(count)
        x_val.append(s)
        for k in range(K_MIN, K_MAX+1):
            avg = sum(curr[k-K_MIN]) / DUPLO
            y_vals[k-K_MIN].append(avg)
            if ERROR_BARS:
                y_stdevs[k-K_MIN].append(math.sqrt(sum([(v - avg)**2 for v in curr[k-K_MIN]]) / DUPLO))

fig, ax = plt.subplots()
for k in range(K_MIN, K_MAX+1):
    if ERROR_BARS:
        ax.errorbar(x_val, y_vals[k-K_MIN], yerr=y_stdevs[k-K_MIN], label="k = "+ str(k))
    else:
        ax.plot(x_val, y_vals[k-K_MIN], label="k = "+ str(k))
if not DEPEND_ON_N:
    ax.set_xlabel("Edge retention probability")
    ax.set_title("Clique distribution for Erdos-Renyi graph of size " + str(SIZE))
else:
    ax.set_xlabel("Graph size")
    ax.set_title("Clique distribution for Erdos-Renyi graphs with p = " + str(P_VAL))
ax.set_ylabel("Clique distribution")
ax.legend()
plt.show()

"""Boundary/combinatorial Laplacian matrix checks"""

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from cliques import *

# Compute boundary map for checks
SIZE = 10 # graph size
P_VAL = 0.3 # edge ret. prob.

G = nx.erdos_renyi_graph(SIZE, P_VAL)
sc = SimplicialCollection()
sc.constructFromGraph(G, return_for_all_edges=True)

for k in range(0, 5):
    if k > 0:
        b = sc.kBoundaryMap(k)
        print("k = " + str(k) + " boundary\n", b)
    else:
        print("k = " + str(k) + " boundary is zero matrix")
    cl = sc.kCombinatorialLaplacian(k)
    print("k = " + str(k) + " combinatorial Laplacian\n", cl)

cl1 = sc.kCombinatorialLaplacian(1)
nullity = matrixNullity(cl1)
print("k = 1 nullity:", nullity, "--> Betti number:", nullity * cl1.shape[0])

pos = nx.spring_layout(G)
nx.draw_networkx(G, pos=pos)
plt.show()

"""Circular dataset generation"""

import matplotlib.pyplot as plt
import random
import math
import networkx as nx

CENTROID = (5, 5)
PUSH_X = 2
PUSH_Y = 2
DEVIATION_X = 1
DEVIATION_Y = 1
N = 300
EPSILON = 0.5

x_val = []
y_val = []

for _ in range(N):
    theta = random.random() * 2 * math.pi
    px, py = math.cos(theta) * PUSH_X, math.sin(theta) * PUSH_Y
    x = CENTROID[0] + px + ((0.5 - random.random()) * DEVIATION_X)
    y = CENTROID[1] + py + ((0.5 - random.random()) * DEVIATION_Y)
    x_val.append(x)
    y_val.append(y)

fig, ax = plt.subplots()
ax.scatter(x_val, y_val)
ax.set_title("Random dataset (N = " + str(N) + ")")
plt.show()

def distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

G = nx.Graph()
pos_dict = {}
for i in range(N):
    G.add_node(i)
    pos_dict[i] = (x_val[i], y_val[i])
    for j in range(i, N): # naive double for loop
        if i != j:
            if distance(x_val[i], y_val[i], x_val[j], y_val[j]) < EPSILON:
                G.add_edge(i, j)

fig, ax = plt.subplots()
nx.draw_networkx(G, pos=pos_dict, with_labels=False, node_size=50, ax=ax)
ax.set_title("Random dataset (N = " + str(N) + ")")
plt.show()
